﻿/**************************************************************************************************************
 * Bai 3: Viết chương trình giải phương trình bậc nhất: ax+b=0, với a, b nhập vào từ bàn phím.
 * Name: Nguyen Gia Nghiem
 * Date: 25/11/2021
 ***********************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Bai3
{
    class Ch4_Bai3
    {
        //BEGIN
        static void Main(string[] args)
        {
            //Khai bao bien
            double a = 0;
            double b = 0;
            double x = 0;
            //Input
            Console.WriteLine("Nhap a: ");
            a = double.Parse(Console.ReadLine());
            Console.WriteLine("Nhap b: ");
            b = double.Parse(Console.ReadLine());
            //Processing
            if (a == 0)
            {
                if (b == 0)
                {
                    //Output
                    Console.WriteLine("Phuong trinh VSN");
                }
                else
                {
                    //Output
                    Console.WriteLine("Phuong trinh VN");
                }
            }
            else
            {
                x = -b / a;
                //Output
                Console.WriteLine("Phuong trinh co nghiem x: " + x);
            }
            //END
        }
    }
}
