﻿/*******************************************************************************************************************
 * Bai 2:2.	Viết chương trình cho phép nhập vào điểm trung bình (ĐTB) của sinh viên. In ra xếp loại của sinh viên. Biết:
 0<=ĐTB<3: Kém; 3 <=ĐTB<5: Yếu; 5 <=ĐTB<6.5: Trung bình khá; 6.5<=ĐTB<8: Khá; 8<=ĐTB< 9: giỏi; 9<=ĐTB<=10: Xuất sắc.
 * Name: Nguyen Gia Nghiem
 * Date: 25/11/2021
 *******************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ch4_Bai2
{
    class Ch4_Bai2
    {
        //BEGIN
        static void Main(string[] args)
        {
            //Khai bao bien
            double dtb = 0;
            //Input
            Console.WriteLine("Nhap diem trung binh: ");
            dtb = double.Parse(Console.ReadLine());
            //Processing
            if (dtb >= 0 && dtb<=10)
            {
                if (dtb < 3)
                {
                    //Output
                    Console.WriteLine("Kem");
                }
                else if (dtb < 5)
                {
                    //Output
                    Console.WriteLine("Yeu");
                }
                else if (dtb < 6.5)
                {
                    //Output
                    Console.WriteLine("Trung binh kha");
                }
                else if ( dtb < 8)
                {
                    //Output
                    Console.WriteLine("Kha");
                }
                else if (dtb >= 8 && dtb < 9)
                {
                    //Output
                    Console.WriteLine("Gioi");
                }
                else
                {
                    //Output
                    Console.WriteLine("Xuat sac");
                }
            }
            else
            {
                //Output
                Console.WriteLine("Khong hop le");
            }
            //END
        }
    }
}
