﻿/************************************************************************************************************************
 * Bai 1: Viết chương trình nhập vào 4 số nguyên. Tìm và in ra số lớn nhất
 * Name: Nguyen Gia Nghiem
 * Date: 25/11/2021
 * *********************************************************************************************************************/

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KTLT_Chuong4
{
    class Ch4_Bai1
    {
        //BEGIN
        static void Main(string[] args)
        {
            //Khai bao bien
            int num1 = 0;
            int num2 = 0;
            int num3 = 0;
            int num4 = 0;
            int max = 0;
            //Input
            Console.WriteLine("Nhap so nguyen thu nhat: ");
            num1 = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen thu hai: ");
            num2 = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen thu ba: ");
            num3 = int.Parse(Console.ReadLine());
            Console.WriteLine("Nhap so nguyen thu tu: ");
            num4 = int.Parse(Console.ReadLine());
            //Processing
            max = num1;
            if (max<num2)
            {
                max = num2;
            }
            if (max<num3)
            {
                max = num3;
            }
            if (max < num4)
            {
                max = num4;
            }
            //Output
            Console.WriteLine("So lon nhat: "+ max);
            //END
        }
    }
}
